from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# Create your views here.
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    items = get_object_or_404(TodoList, id=id)
    context = {
        "todo_items": items,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            TodoList = form.save()
            return redirect("todo_list_detail", id=TodoList.id)

    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=update.id)

    else:
        form = TodoListForm(instance=update)

    context = {
        "update_list": update,
        "todolist_form": form,
    }

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    deletelist = TodoList.objects.get(id=id)
    if request.method == "POST":
        deletelist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            TodoItem = form.save()
            # TodoItem.save()  OR THIS?
            return redirect("todo_list_detail", id=TodoItem.list.id)

    else:
        form = TodoItemForm()

    context = {"todoitem_form": form}

    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    change_item = TodoItem.objects.get(id=id)
    # new_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=change_item)
        if form.is_valid():
            newlist = form.cleaned_data["list"]
            form.save()
            return redirect("todo_list_detail", id=newlist.id)

    else:
        form = TodoItemForm(instance=change_item)

    context = {
        "todoitem_form": form,
        "change_item": change_item,
    }

    return render(request, "todos/update_item.html", context)
